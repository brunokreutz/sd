package com.chatt.demo;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.chatt.demo.custom.CustomActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import static android.content.Context.MODE_PRIVATE;

/**
 * The Class UserList is the Activity class. It shows a list of all users of
 * this app. It also shows the Offline/Online status of users.
 */
public class UserList extends CustomActivity
{

	/** The Chat list. */
	private ArrayList<String> uList;

	private UserAdapter adp;
	/** The user. */
	public static String user;

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_list);

		getActionBar().setDisplayHomeAsUpEnabled(false);

		Log.e("usuario", ((ChattApp)getApplicationContext()).getUsuario());

//		updateUserStatus(true);
//
//		setupConnectionFactory();
//		publishToAMQP();
//		//setupPubButton();
//		publishMessage("texte");
//
//		final Handler incomingMessageHandler = new Handler() {
//			@Override
//			public void handleMessage(Message msg) {
//				String message = msg.getData().getString("msg");
//				//TextView tv = (TextView) findViewById(R.id.textView);
//				Date now = new Date();
//				SimpleDateFormat ft = new SimpleDateFormat ("hh:mm:ss");
//				//tv.append(ft.format(now) + ' ' + message + '\n');
//				Log.d("resposta",message);
//			}
//		};
//		subscribe(incomingMessageHandler);
	}
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_addContato:
				//criar dialog para usuario inserir nome(fila) do contato
				if(uList.size() == 1 && uList.get(0).equals("Sem dados"))
					uList.remove(0);
				showDialogAdd();
				return true;

			case R.id.menu_sair:
				showDialogExit();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void addContatoToList(String nome){
		uList.add(nome);
		ListView listView = (ListView) findViewById(R.id.list);
		listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, uList));

		Type listOfStringObject = new TypeToken<List<String>>(){}.getType();
		Gson gson = new Gson();
		String json = gson.toJson(uList, listOfStringObject);

		SharedPreferences sharedPreferences = this.getSharedPreferences("chatsd", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString("listaContatos", json);
		editor.commit();
	}

	private void showDialogAdd(){
		AlertDialog.Builder b = new AlertDialog.Builder(this);
		final EditText t = new EditText(this);
		t.setText("");
		b.setMessage("Inserir nome da fila do usuário.");
		b.setView(t);
		b.setPositiveButton("Add", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				addContatoToList(t.getText().toString());
			}
		});
		b.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		AlertDialog dia = b.create();
		dia.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		dia.show();
	}

	private void showDialogExit(){
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage("Deseja sair?")
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						boolean sucesso = limparDadosUsuario();
						if(sucesso){
							irTelaLogin();
						}else{

						}
					}
				})
				.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// do nothing
					}
				})
				.setIcon(android.R.drawable.ic_dialog_alert)
				.show();

	}

	private boolean limparDadosUsuario(){
		//limpar sharedPreference e conversas no banco
		try{
			SharedPreferences sharedPreferences = this.getSharedPreferences("chatsd", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.clear();
			editor.commit();

			//limpar Banco

			return true;
		}catch (Exception e){
			Log.e("erro", e.getMessage());
			return false;
		}
	}

	private void irTelaLogin(){
		Intent intent = new Intent(getApplicationContext(), Login.class);
		startActivity(intent);
		finish();
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();

//		updateUserStatus(false);

		//publishThread.interrupt();
		//subscribeThread.interrupt();
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume()
	{
		super.onResume();
		loadUserList();

	}

	/**
	 * Update user status.
	 * 
	 * @param online
	 *            true if user is online
	 */
	private void updateUserStatus(boolean online)
	{
//		user.put("online", online);
//		user.saveEventually();
	}

	/**
	 * Load list of users.
	 */
	private void loadUserList() {
//		final ProgressDialog dia = ProgressDialog.show(this, null, getString(R.string.alert_loading)); 

		user = ((ChattApp)getApplicationContext()).getUsuario();
		SharedPreferences sharedPreferences = getSharedPreferences("chatsd", Context.MODE_PRIVATE);
		String userList = sharedPreferences.getString("listaContatos", "");

		Type listOfStringObject = new TypeToken<List<String>>(){}.getType();
		Gson gson = new Gson();
		uList = gson.fromJson(userList, listOfStringObject);
		if(uList == null){
			Log.e("userList", "lista null");
			uList = new ArrayList<String>();
			uList.add("Sem dados");
		}

		ListView listView = (ListView) findViewById(R.id.list);
		listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, uList));
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Intent intent = new Intent(getApplicationContext(), Chat.class);
				intent.putExtra("numero", uList.get(i));
				startActivity(intent);
			}
		});

	}

	/**
	 * The Class UserAdapter is the adapter class for User ListView. This
	 * adapter shows the user name and it's only online status for each item.
	 */
	private class UserAdapter extends ArrayAdapter<String> {

		private List<String> list;

		public UserAdapter(Context context, int resource, List<String> objects) {
			super(context, resource, objects);
			this.list = objects;
		}

		/* (non-Javadoc)
                 * @see android.widget.Adapter#getItemId(int)
                 */
		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int pos, View v, ViewGroup arg2)
		{
			if (v == null)
				v = getLayoutInflater().inflate(R.layout.chat_item, null);

			String c = uList.get(pos);
			TextView lbl = (TextView) v;
			lbl.setText(c);
			lbl.setCompoundDrawablesWithIntrinsicBounds(
					true ? R.drawable.ic_online
							: R.drawable.ic_offline, 0, R.drawable.arrow, 0);

			return v;
		}



	}
}
