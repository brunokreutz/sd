package com.chatt.demo;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity {

    private MessageConsumer mConsumer;
    private TextView mOutput;
    private String QUEUE_NAME = "bye";
    private String EXCHANGE_NAME = "logs";
    private String message = "";
    private String name = "";
    private Thread subscribeThread;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {


        if(((ChattApp) getApplicationContext()).getUsuario() != null && !((ChattApp) getApplicationContext()).getUsuario().equals("default")){
            Intent intent = new Intent(getApplicationContext(), Login.class);
            startActivity(intent);
            finish();
        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Toast.makeText(MainActivity.this, "RabbitMQ Chat Service!", Toast.LENGTH_LONG).show();


        final Handler incomingMessageHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String message = msg.getData().getString("msg");
                EditText tv = (EditText) findViewById(R.id.output);
                Date now = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
                tv.append(ft.format(now) + ' ' + message + '\n');
            }
        };
        subscribe(incomingMessageHandler);

        final EditText etv = (EditText) findViewById(R.id.out2);
        etv.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    message = name + ": " + etv.getText().toString();
                    new send().execute(message);
                    etv.setText("");
                    return true;
                }
                return false;
            }
        });

        Button btn = (Button)findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = etv.getText().toString();
                etv.setText("");
                new send().execute(name);


            }
        });



//The output TextView we'll use to display messages
        mOutput = (TextView) findViewById(R.id.output);


    }

    private class send extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... Message) {
            try {

                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost("rodolfoliver.koding.io");

//my internet connection is a bit restrictive so I have use an external server
// which has RabbitMQ installed on it. So I use "setUsername" and "setPassword"
                factory.setUsername("guest");
                factory.setPassword("guest");
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel();
                channel.exchangeDeclare(EXCHANGE_NAME, "fanout", true);
                channel.queueDeclare(QUEUE_NAME, false, false, false, null);
                String tempstr = "";
                for (int i = 0; i < Message.length; i++)
                    tempstr += Message[i];

                channel.basicPublish(EXCHANGE_NAME, QUEUE_NAME, null, tempstr.getBytes());

                channel.close();

                connection.close();


            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            // TODO Auto-generated method stub
            return null;
        }

    }

    void subscribe(final Handler handler){
        subscribeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        ConnectionFactory factory = new ConnectionFactory();
                        factory.setHost("rodolfoliver.koding.io");

//my internet connection is a bit restrictive so I have use an external server
// which has RabbitMQ installed on it. So I use "setUsername" and "setPassword"
                        factory.setUsername("guest");
                        factory.setPassword("guest");
                        Connection connection = factory.newConnection();
                        Channel channel = connection.createChannel();
                        channel.basicQos(1);
                        AMQP.Queue.DeclareOk q = channel.queueDeclare();
                        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "");
                        QueueingConsumer consumer = new QueueingConsumer(channel);
                        channel.basicConsume(QUEUE_NAME, true, consumer);

                        while (true) {
                            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                            String message = new String(delivery.getBody());
                            Log.d("","[r] " + message);
                            Message msg = handler.obtainMessage();
                            Bundle bundle = new Bundle();
                            bundle.putString("msg", message);
                            msg.setData(bundle);
                            handler.sendMessage(msg);
                        }
                    } catch (InterruptedException e) {
                        break;
                    } catch (Exception e1) {
                        Log.d("", "Connection broken: " + e1.getClass().getName());
                        try {
                            Thread.sleep(5000); //sleep and then try again
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                }
            }
        });
        subscribeThread.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mConsumer.connectToRabbitMQ();
    }

    @Override
    protected void onPause() {
        super.onPause();
   //     mConsumer.dispose();
    }
}