package com.chatt.demo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.EditText;

import com.chatt.demo.custom.CustomActivity;
import com.chatt.demo.utils.Utils;

public class Login extends CustomActivity {

	private EditText user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		//Testa se ja tem usuario no app, se tiver segue para tela de contatos sem fazer "login"
		if(((ChattApp) getApplicationContext()).getUsuario() != null && !((ChattApp) getApplicationContext()).getUsuario().equals("default")){
			Intent intent = new Intent(getApplicationContext(), UserList.class);
			startActivity(intent);
			finish();
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		setTouchNClick(R.id.btnLogin);
		setTouchNClick(R.id.btnReg);

		user = (EditText) findViewById(R.id.user);

		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		user.setText(telephonyManager.getLine1Number());

	}

	@Override
	public void onClick(View v)
	{
		super.onClick(v);
		if (v.getId() == R.id.btnReg) {
			startActivityForResult(new Intent(this, Register.class), 10);
		}
		else {

			String u = user.getText().toString();

			if (u.length() == 0) {
				Utils.showDialog(this, R.string.err_fields_empty);
				return;
			}

			final ProgressDialog dia = ProgressDialog.show(this, null, getString(R.string.alert_wait));
			SharedPreferences sharedPreferences = this.getSharedPreferences("chatsd", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString("usuario", u);
			editor.commit();

			Intent intent = new Intent(getApplicationContext(), UserList.class);
			startActivity(intent);
			dia.dismiss();
			finish();
		}
	}

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}
