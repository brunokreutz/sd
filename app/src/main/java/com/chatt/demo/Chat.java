package com.chatt.demo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.text.InputType;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chatt.demo.custom.CustomActivity;
import com.chatt.demo.model.Conversation;
import com.chatt.demo.utils.Const;
import com.chatt.demo.utils.Conversa;
import com.chatt.demo.utils.ConversaDAO;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * The Class Chat is the Activity class that holds main chat screen. It shows
 * all the conversation messages between two users and also allows the user to
 * send and receive messages.
 */
public class Chat extends CustomActivity
{

	/** The Conversation list. */
	private ArrayList<Conversation> convList;

	/** The chat adapter. */
	private ChatAdapter adp;

	/** The Editext to compose the message. */
	private EditText txt;

	/** The user name of buddy. */
	private String buddy;

	/** The date of last message in conversation. */
	private Date lastMsgDate;

	/** Flag to hold if the activity is running or not. */
	private boolean isRunning;

	/** The handler. */
	private static Handler handler;
	private MessageConsumer mConsumer;
	private TextView mOutput;
	private String QUEUE_NAME = "bye";
	private String EXCHANGE_NAME = "logs";
	private String message = "";
	private String name = "";
	private Thread subscribeThread;
	private String numero;
	private String usuario;
	private int contador;
	private SharedPreferences sharedPreferences;
	private String user;

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		setContentView(R.layout.chat);

		convList = new ArrayList<Conversation>();
		ListView list = (ListView) findViewById(R.id.list);
		adp = new ChatAdapter();
		list.setAdapter(adp);
		list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		list.setStackFromBottom(true);

		txt = (EditText) findViewById(R.id.txt);
		txt.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_FLAG_MULTI_LINE);

		setTouchNClick(R.id.btnSend);

		buddy = getIntent().getStringExtra(Const.EXTRA_DATA);
		getActionBar().setTitle(buddy);

		handler = new Handler();

		//Começa

		sharedPreferences = getSharedPreferences("chatsd", Context.MODE_PRIVATE);
		contador = sharedPreferences.getInt("contadorMsg", 0);
		user = sharedPreferences.getString("usuario", "default");

		Intent intent = getIntent();
		numero = intent.getStringExtra("numero");
		getActionBar().setTitle(numero);

		SharedPreferences sharedPreferences = getSharedPreferences("chatsd", Context.MODE_PRIVATE);
		usuario = sharedPreferences.getString("usuario", "default");


		//Toast.makeText(MainActivity.this, "RabbitMQ Chat Service!", Toast.LENGTH_LONG).show();


		final Handler incomingMessageHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				String message = msg.getData().getString("msg");
				//EditText tv = (EditText) findViewById(R.);
				String[] messages = message.split(":!");

				if(messages[0].equals(user)){
					for(Conversation c : convList){
						if(c.getId() == Integer.parseInt(messages[2])){
							c.setStatus(Conversation.STATUS_SENT);
							break;
						}
					}
				}else{
					Date now = new Date();
					SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
					Conversation conversation = new Conversation(messages[1], now, messages[0], Integer.parseInt(messages[2]));
					convList.add(conversation);
				}
				adp.notifyDataSetChanged();
				saveConv();
			}
		};
		subscribe(incomingMessageHandler);

		//mOutput = (TextView) findViewById(R.id.output);


	}



	private class send extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... Message) {
			try {

				ConnectionFactory factory = new ConnectionFactory();
				factory.setHost("rodolfoliver.koding.io");

//my internet connection is a bit restrictive so I have use an external server
// which has RabbitMQ installed on it. So I use "setUsername" and "setPassword"
				factory.setUsername("guest");
				factory.setPassword("guest");
				Connection connection = factory.newConnection();
				Channel channel = connection.createChannel();
				channel.exchangeDeclare(EXCHANGE_NAME, "fanout", true);
				channel.queueDeclare(numero, false, false, false, null);
				String tempstr = "";
				for (int i = 0; i < Message.length; i++)
					tempstr += Message[i];

				channel.basicPublish(EXCHANGE_NAME, numero, null, tempstr.getBytes());

				channel.close();

				connection.close();


			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// TODO Auto-generated method stub
			return null;
		}

	}

	void subscribe(final Handler handler){
		subscribeThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true) {
					try {
						ConnectionFactory factory = new ConnectionFactory();
						factory.setHost("rodolfoliver.koding.io");

//my internet connection is a bit restrictive so I have use an external server
// which has RabbitMQ installed on it. So I use "setUsername" and "setPassword"
						factory.setUsername("guest");
						factory.setPassword("guest");
						Connection connection = factory.newConnection();
						Channel channel = connection.createChannel();
						channel.basicQos(1);
						AMQP.Queue.DeclareOk q = channel.queueDeclare();
						channel.queueBind(usuario, EXCHANGE_NAME, "");
						QueueingConsumer consumer = new QueueingConsumer(channel);
						channel.basicConsume(usuario, true, consumer);

						while (true) {
							QueueingConsumer.Delivery delivery = consumer.nextDelivery();
							//channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
							String message = new String(delivery.getBody());
							Log.d("", "[r] " + message);
							Message msg = handler.obtainMessage();
							Bundle bundle = new Bundle();
							bundle.putString("msg", message);
							msg.setData(bundle);
							handler.sendMessage(msg);

						}
					} catch (InterruptedException e) {
						break;
					} catch (Exception e1) {
						Log.d("", "Connection broken: " + e1.getClass().getName());
						try {
							Thread.sleep(5000); //sleep and then try again
						} catch (InterruptedException e) {
							break;
						}
					}
				}
			}
		});
		subscribeThread.start();
	}



	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume()
	{
		super.onResume();
		isRunning = true;
		loadConversationList();
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onPause()
	 */
	@Override
	protected void onPause()
	{
		super.onPause();
		isRunning = false;
	}

	/* (non-Javadoc)
	 * @see com.socialshare.custom.CustomFragment#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v)
	{
		super.onClick(v);
		if (v.getId() == R.id.btnSend)
		{
			sendMessage();
		}

	}

	/**
	 * Call this method to Send message to opponent. It does nothing if the text
	 * is empty otherwise it creates a Parse object for Chat message and send it
	 * to Parse server.
	 */
	private void sendMessage()
	{
		if (txt.length() == 0)
			return;

		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(txt.getWindowToken(), 0);

		String s = usuario + ":!" + txt.getText().toString() + ":!" + contador;
		new send().execute(s);
		final Conversation c = new Conversation(txt.getText().toString(), new Date(), usuario, contador);
		c.setStatus(Conversation.STATUS_SENDING);
		convList.add(c);
		adp.notifyDataSetChanged();
		txt.setText(null);
		contador++;
		saveConv();
	}

	private void saveConv(){
		ConversaDAO conversaDAO = new ConversaDAO(getApplicationContext());
		Conversa conversa = new Conversa(Integer.valueOf(numero),convList);
		conversaDAO.salvar(conversa);
	}

	/**
	 * Load the conversation list from Parse server and save the date of last
	 * message that will be used to load only recent new messages
	 */
	private void loadConversationList(){
		ConversaDAO conversaDAO = new ConversaDAO(getApplicationContext());
		JSONObject jsonObject = null;
		try {
			if (conversaDAO.recuperarConversa(Integer.valueOf(numero)) != null) {
				Conversa c = conversaDAO.recuperarConversa(Integer.valueOf(numero));

				ArrayList<String> ac = conversaDAO.recuperarTodos(Integer.valueOf(numero));
				for (String string:ac){
					Log.d("lista",string);
					Conversation conversation = new Conversation(string);
					convList.add(conversation);
				}

				if(convList!= null && convList.size() != 0){
					contador = convList.get(convList.size()-1).getId();
				}else{
					contador = 0;
				}


				adp.notifyDataSetChanged();
				jsonObject = new JSONObject();
				jsonObject.put("int", c.getId());
				jsonObject.put("conversa", c.getConversa());
				Log.d("ac",ac.toString());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (jsonObject!= null)
			Log.d("json",jsonObject.toString());
		else
			Log.d("json","nil");

	}

	/**
	 * The Class ChatAdapter is the adapter class for Chat ListView. This
	 * adapter shows the Sent or Receieved Chat message in each list item.
	 */
	private class ChatAdapter extends BaseAdapter
	{

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount()
		{
			return convList.size();
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Conversation getItem(int arg0)
		{
			return convList.get(arg0);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int pos, View v, ViewGroup arg2)
		{
			Conversation c = getItem(pos);
			SharedPreferences sharedPreferences = getSharedPreferences("chatsd", Context.MODE_PRIVATE);
			String user = sharedPreferences.getString("usuario", "default");
			Log.d("usr",user);
			Log.d("sender", c.getSender());
			if (c.getSender().equals(user)) {
				v = getLayoutInflater().inflate(R.layout.chat_item_sent, null);
				Log.d("send", "send");
			}
			else {
				v = getLayoutInflater().inflate(R.layout.chat_item_rcv, null);
				Log.d("receive","receive");
			}
			TextView lbl = (TextView) v.findViewById(R.id.lbl1);
			if (c.getDate()!= null)
				lbl.setText(DateUtils.getRelativeDateTimeString(Chat.this, c
					.getDate().getTime(), DateUtils.SECOND_IN_MILLIS,
					DateUtils.DAY_IN_MILLIS, 0));

			lbl = (TextView) v.findViewById(R.id.lbl2);
			lbl.setText(c.getMsg());

			lbl = (TextView) v.findViewById(R.id.lbl3);
			if (c.getSender().equals(user))
			{
				if (c.getStatus() == Conversation.STATUS_SENT)
					lbl.setText("Delivered");
				else if (c.getStatus() == Conversation.STATUS_SENDING)
					lbl.setText("Sending...");
				else
					lbl.setText("Failed");
			}
			else
				lbl.setText("");

			return v;
		}

	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == android.R.id.home)
		{
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
