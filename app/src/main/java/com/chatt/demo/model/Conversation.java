package com.chatt.demo.model;

import android.util.Log;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.chatt.demo.ChattApp;
import com.chatt.demo.UserList;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/**
 * The Class Conversation is a Java Bean class that represents a single chat
 * conversation message.
 */
public class Conversation
{

	/** The Constant STATUS_SENDING. */
	public static final int STATUS_SENDING = 0;

	/** The Constant STATUS_SENT. */
	public static final int STATUS_SENT = 1;

	/** The Constant STATUS_FAILED. */
	public static final int STATUS_FAILED = 2;
	/** The msg. */
	private String msg;

	/** The status. */
	private int status = STATUS_SENT;

	/** The date. */
	private Date date;

	/** The sender. */
	private String sender;

	private int senderInt;

	private int id;


	/**
	 * Instantiates a new conversation.
	 * 
	 * @param msg
	 *            the msg
	 * @param date
	 *            the date
	 * @param sender
	 *            the sender
	 */
	public Conversation(String msg, Date date, String sender, int id)
	{
		this.msg = msg;
		this.date = date;
		this.sender = sender;
		this.id = id;
	}

	public JSONObject getJSONObject() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("msg", msg);
			obj.put("date", date);
			obj.put("sender", sender);
			obj.put("idConversa",id);
		} catch (JSONException e) {
			Log.e("JSONException: " , e.getMessage());
		}
		return obj;
	}

	public Conversation (String jsonString){
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			this.msg = jsonObject.get("msg").toString();
			//DateFormat format = new SimpleDateFormat();
			//this.date = format.parse(jsonObject.get("date").toString());
			this.date = getDate();
			this.sender = jsonObject.get("sender").toString();
			this.id = jsonObject.getInt("idConversa");

		} catch (JSONException e) {
			e.printStackTrace();
		//} catch (ParseException e) {
		//	e.printStackTrace();
		}
	}


	/**
	 * Instantiates a new conversation.
	 */
	public Conversation()
	{
	}

	/**
	 * Gets the msg.
	 * 
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}

	/**
	 * Sets the msg.
	 * 
	 * @param msg
	 *            the new msg
	 */
	public void setMsg(String msg)
	{
		this.msg = msg;
	}

	/**
	 * Checks if is sent.
	 * 
	 * @return true, if is sent
	 */
	public boolean isSent()
	{
		//return ((ChattApp)getApplicationContext()).getUsuario().equals(sender);
		return true;
	}

	/**
	 * Gets the date.
	 * 
	 * @return the date
	 */
	public Date getDate()
	{
		return date;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            the new date
	 */
	public void setDate(Date date)
	{
		this.date = date;
	}

	/**
	 * Gets the sender.
	 * 
	 * @return the sender
	 */
	public String getSender()
	{
		return sender;
	}

	/**
	 * Sets the sender.
	 * 
	 * @param sender
	 *            the new sender
	 */
	public void setSender(String sender)
	{
		this.sender = sender;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
