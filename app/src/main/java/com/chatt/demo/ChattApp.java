package com.chatt.demo;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;


/**
 * The Class ChattApp is the Main Application class of this app. The onCreate
 * method of this class initializes the Parse.
 */
public class ChattApp extends Application {


	private String usuario;

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		SharedPreferences sharedPreferences = getSharedPreferences("chatsd", Context.MODE_PRIVATE);
		usuario = sharedPreferences.getString("usuario","default");

	}

	public String getUsuario(){
		SharedPreferences sharedPreferences = getSharedPreferences("chatsd", Context.MODE_PRIVATE);
		String user = sharedPreferences.getString("usuario","default");
		Log.e("usuario", user);
		return user;
	}

}
