package com.chatt.demo.utils;

/**
 * Created by kreutz on 15-12-20.
 */
 import android.content.Context;
 import android.database.sqlite.SQLiteDatabase;
 import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by yuri on 19/12/15.
 */
public class PersistenceHelper extends SQLiteOpenHelper {

    public static final String NOME_BANCO =  "CONVERSA";
    public static final int VERSAO =  1;

    private static PersistenceHelper instance;

    private PersistenceHelper(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
    }

    public static PersistenceHelper getInstance(Context context) {
        if(instance == null)
            instance = new PersistenceHelper(context);

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ConversaDAO.SCRIPT_CRIACAO_TABELA_CHAT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(ConversaDAO.SCRIPT_DELECAO_TABELA);
        onCreate(db);
    }

}
