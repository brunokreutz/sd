package com.chatt.demo.utils;

import com.chatt.demo.model.Conversation;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by kreutz on 15-12-20.
 */
public class Conversa {
    public void setId(int id) {
        this.id = id;
    }

    public void setConversa(String conversa) {
        this.conversa = conversa;
    }

    private int id;

    public Conversa() {
    }

    private String conversa;




    public Conversa(int id, ArrayList<Conversation> conversa){
        JSONArray jsonArray = new JSONArray();
        for (int i=0; i < conversa.size(); i++) {
            jsonArray.put(conversa.get(i).getJSONObject());
        }
        this.id = id;
        this.conversa = jsonArray.toString();
    }

    public int getId(){
        return this.id;
    }

    public String getConversa(){
        return this.conversa;
    }



}
