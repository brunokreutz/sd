package com.chatt.demo.utils;

/**
 * Created by kreutz on 15-12-20.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.chatt.demo.model.Conversation;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuri on 19/12/15.
 */
public class ConversaDAO{

    public static final String NOME_TABELA = "Chat";
    public static final String COLUNA_ID = "id";
    public static final String COLUNA_CONVERSA = "conversa";


    public static final String SCRIPT_CRIACAO_TABELA_CHAT = "CREATE TABLE " + NOME_TABELA + "("
            + COLUNA_ID + " INTEGER PRIMARY KEY," + COLUNA_CONVERSA + " TEXT" + ")";

    public static final String SCRIPT_DELECAO_TABELA =  "DROP TABLE IF EXISTS " + NOME_TABELA;


    private SQLiteDatabase dataBase = null;


    private static ConversaDAO instance;

    public static ConversaDAO getInstance(Context context) {
        if(instance == null)
            instance = new ConversaDAO(context);
        return instance;
    }

    public ConversaDAO(Context context) {
        PersistenceHelper persistenceHelper = PersistenceHelper.getInstance(context);
        dataBase = persistenceHelper.getWritableDatabase();
    }

    public void salvar(Conversa chat) {
        ContentValues values = gerarContentValeuesChat(chat);
        Conversa c = recuperarConversa(chat.getId());
        if (c != null)
            editar(chat);
        else
            dataBase.insert(NOME_TABELA, null, values);

    }

    public ArrayList<String> recuperarTodos(int id) {
        String queryReturnAll = "SELECT * FROM " + NOME_TABELA
        + " WHERE " + COLUNA_ID + " = ?";

        Cursor cursor = dataBase.rawQuery(queryReturnAll, new String[] { id + "" });
        ArrayList<String> chats = construirChatPorCursor(cursor);

        return chats;
    }

    public Conversa recuperarConversa(int id){
        Log.d("id:",String.valueOf(id));
        Conversa conversa = null;

        String sql = "SELECT * FROM " + NOME_TABELA
                + " WHERE " + COLUNA_ID + " = ?";

        Cursor cursor = dataBase.rawQuery(sql, new String[] { id + "" });

        if (cursor.moveToNext()) {
            conversa = new Conversa();
            conversa.setId(cursor.getInt(0));
            conversa.setConversa(cursor.getString(1));
            Log.d("msg",conversa.getConversa());
        }
        return conversa;
    }


    public void deletar(Conversa chat) {

        String[] valoresParaSubstituir = {
                String.valueOf(chat.getId())
        };

        dataBase.delete(NOME_TABELA, COLUNA_ID + " =  ?", valoresParaSubstituir);
    }

    public void editar(Conversa chat) {
        ContentValues valores = gerarContentValeuesChat(chat);

        String[] valoresParaSubstituir = {
                String.valueOf(chat.getId())
        };

        dataBase.update(NOME_TABELA, valores, COLUNA_ID + " = ?", valoresParaSubstituir);
    }

    public void fecharConexao() {
        if(dataBase != null && dataBase.isOpen())
            dataBase.close();
    }


    private ArrayList<String> construirChatPorCursor(Cursor cursor) {
        ArrayList<String> chats = new ArrayList<String>();
        if(cursor == null)
            return chats;

        try {

            if (cursor.moveToFirst()) {
                do {

                    int indexID = cursor.getColumnIndex(COLUNA_ID);
                    int indexConversa = cursor.getColumnIndex(COLUNA_CONVERSA);
                    int id = cursor.getInt(indexID);
                    String conv = cursor.getString(indexConversa);

                    JSONArray jArray = new JSONArray(conv);
                    if (jArray != null) {
                        for (int i=0;i<jArray.length();i++){
                            chats.add(jArray.get(i).toString());
                        }
                    }
                    ;



                } while (cursor.moveToNext());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return chats;
    }

    private ContentValues gerarContentValeuesChat(Conversa chat) {
        ContentValues values = new ContentValues();
        values.put(COLUNA_CONVERSA, chat.getConversa());
        values.put(COLUNA_ID, chat.getId());

        return values;
    }
}